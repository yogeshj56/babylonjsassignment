import logo from './logo.svg';
import './App.css';
import SceneComponent from './SceneComponent.tsx';

function App() {
  return (
    <div className = "Babylon"> 
        <SceneComponent></SceneComponent>
    </div>
  );
}

export default App;
