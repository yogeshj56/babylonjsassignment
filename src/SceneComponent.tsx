/*-- import statements --*/
import React, { Component } from "react";
import * as BABYLON from "@babylonjs/core";
import "@babylonjs/loaders";

//state
interface IState {

}

// props
interface IProps {
	
}

//global variables
var canvas: any;
var engine: BABYLON.Engine;
var scene: BABYLON.Scene;
var count = 1;
var modelURL = "https://drive.google.com/file/d/1c2GU-xApwhKUfmdNNW1OxZVWUp_bNJPE/view?usp=sharing";

export default class <SceneComponent> extends Component<IProps, IState>{
	//code

	constructor(props: IProps) {
		super(props);

		this.state = {

		};

	};
	
	/*************************************************
	*
	* React Life Cycle method calls immediately after application born. Here Acts as Initialization method
	*
	*************************************************/
	//Component Did Mount
	componentDidMount() {
		//code
		
		if(count == 1)
		{
			this.initialize();
			count++;
		}

	};

	/*************************************************
	*
	* React Life Cycle method calls when the state of the component changes.
	*
	*************************************************/
	componentDidUpdate(prevProps : any, prevState : any) {

	};

	/*************************************************
	*
	* Functions
	*
	*************************************************/
	initialize = () => {
		console.clear();

		canvas = document.getElementById("canvas") as HTMLCanvasElement;
		console.log('canvas created successfuully');

		engine = new BABYLON.Engine(canvas, true, {doNotHandleContextLost:true}, true);
		console.log('Engine initialized succesfully', engine);
		
		document.addEventListener("keydown", (event) => {
			this.keyDown(event);
		});

		this.createScene();
		this.renderLoop();
	}

	keyDown = (event) => {
		console.log(event);
		
		//Switch case for other Keys
		switch (event.key) {
			case "W":
			case "w":
			case "ArrowUp":
				
			break;
			default:
				break;
		}
		
	}

	createScene = () => {

        scene = new BABYLON.Scene(engine);

        const camera = new BABYLON.ArcRotateCamera("camera", 
		0, Math.PI/2,
		5, 
		new BABYLON.Vector3(0,1,0), 
		scene);
		//camera.setPosition(new BABYLON.Vector3(0, 0, -10));
        camera.attachControl();

        const hemiLight = new BABYLON.HemisphericLight("hemiLight", new BABYLON.Vector3(0,1,0), scene);
        hemiLight.intensity = 0.5;

        const ground = BABYLON.MeshBuilder.CreateGround("ground", {width:10, height:10}, scene);
		ground.position = new BABYLON.Vector3(0, -0.5, 0);

		// Load glb model
		var model = BABYLON.SceneLoader.ImportMesh("","/models/","Chair.glb", scene, ()=>{
		});
		console.log("check1 = ", BABYLON.SceneLoader.ImportMesh);
		
    }

	renderLoop = () => {
		engine.runRenderLoop(()=>{
			scene.render();
		})
	}

	/*************************************************
	*
	* React's Render Method
	*
	*************************************************/
	render() {
		return(
			
			<div className="App">
				<canvas id = "canvas" />
     
    		</div>
			
		);
	};
}
